FROM r-base

# Build:
# sudo docker build -t r-cantal-dev .
# Run a terminal:
# sudo docker run -ti --rm -v "$PWD":/home/docker -w /home/docker r-cantal-dev bash
# or directly in R:
# sudo docker run -ti --rm -v "$PWD":/home/docker -w /home/docker r-cantal-dev
# First command to ensure everything is installed:
# devtools::install_deps(upgrade="never", dependencies = TRUE)

RUN apt-get update
RUN apt-get install --yes libgeos-dev libgmp-dev
RUN apt-get install --yes pandoc-citeproc r-cran-testthat r-cran-devtools r-cran-raster r-cran-ggplot2 r-cran-igraph r-cran-rmarkdown r-cran-fields r-cran-rsqlite r-cran-maptools r-cran-rgl r-cran-rgdal r-cran-sf r-cran-bh r-cran-vegan r-cran-ape r-cran-fastmatch r-cran-geometry r-cran-dosnow r-cran-itertools r-cran-sass r-cran-jquerylib
RUN R -e 'install.packages(c("OCNet", "SSN", "betapart", "jquerylib", "bslib","rgl"))'
# Install rgl from R for avoiding warnings
